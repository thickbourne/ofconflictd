package org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.ofconflictd.impl.rev141210.modules.module.configuration;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.ofconflictd.impl.rev141210.modules.module.configuration.ofconflictd.Broker;
import org.opendaylight.yangtools.yang.common.QName;
import org.opendaylight.yangtools.yang.binding.DataObject;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.controller.config.rev130405.modules.module.Configuration;
import org.opendaylight.yangtools.yang.binding.Augmentable;


/**
 * &lt;p&gt;This class represents the following YANG schema fragment defined in module &lt;b&gt;ofconflictd-impl&lt;/b&gt;
 * &lt;br&gt;(Source path: &lt;i&gt;META-INF/yang/ofconflictd-impl.yang&lt;/i&gt;):
 * &lt;pre&gt;
 * case ofconflictd {
 *     container broker {
 *         leaf type {
 *             type leafref;
 *         }
 *         leaf name {
 *             type leafref;
 *         }
 *         uses service-ref {
 *             refine (urn:opendaylight:params:xml:ns:yang:ofconflictd:impl?revision=2014-12-10)type {
 *                 leaf type {
 *                     type leafref;
 *                 }
 *             }
 *         }
 *     }
 * }
 * &lt;/pre&gt;
 * The schema path to identify an instance is
 * &lt;i&gt;ofconflictd-impl/modules/module/configuration/(urn:opendaylight:params:xml:ns:yang:ofconflictd:impl?revision=2014-12-10)ofconflictd&lt;/i&gt;
 *
 */
public interface Ofconflictd
    extends
    DataObject,
    Augmentable<org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.ofconflictd.impl.rev141210.modules.module.configuration.Ofconflictd>,
    Configuration
{



    public static final QName QNAME = org.opendaylight.yangtools.yang.common.QName.create("urn:opendaylight:params:xml:ns:yang:ofconflictd:impl",
        "2014-12-10", "ofconflictd").intern();

    Broker getBroker();

}

