/*
 * Copyright © 2015 Janakarajan Natarajan and others.  All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */
package org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.ofconflictd.impl.rev141210;

import org.junit.Test;

public class OfconflictdModuleFactoryTest {
    @Test
    public void testFactoryConstructor() {
        // ensure no exceptions on construction
        new OfconflictdModuleFactory();
    }
}
