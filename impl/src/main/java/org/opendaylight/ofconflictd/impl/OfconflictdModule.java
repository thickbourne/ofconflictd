package org.opendaylight.ofconflictd.impl;

import org.opendaylight.controller.md.sal.binding.api.DataBroker;
import org.opendaylight.controller.md.sal.binding.api.DataChangeListener;
import org.opendaylight.controller.md.sal.common.api.data.AsyncDataChangeEvent;
import org.opendaylight.yangtools.yang.binding.DataObject;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.net.Socket;
import java.net.UnknownHostException;
import java.io.IOException;

public class OfconflictdModule implements DataChangeListener {
    private static Logger LOG = LoggerFactory.getLogger(OfconflictdModule.class);
    private DataBroker broker = null;

    public OfconflictdModule(DataBroker broker) {
        this.broker = broker;
    }

    public void onDataChanged(final AsyncDataChangeEvent<InstanceIdentifier<?>, DataObject> change) {
        LOG.info("DATA HAS CHANGED");
        try {
            Socket s = new Socket("127.0.0.1", 9090);
        } catch (UnknownHostException e) {
            LOG.info("NO SERVER");
        } catch (IOException e) {
            LOG.info("IO ERROR");
        }
    }
}
