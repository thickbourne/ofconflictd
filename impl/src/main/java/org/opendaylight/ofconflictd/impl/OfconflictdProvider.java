/*
 * Copyright © 2015 Janakarajan Natarajan and others.  All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */
package org.opendaylight.ofconflictd.impl;

import org.opendaylight.controller.sal.binding.api.BindingAwareBroker.ProviderContext;
import org.opendaylight.controller.sal.binding.api.BindingAwareProvider;
import org.opendaylight.controller.md.sal.common.api.data.LogicalDatastoreType;
import org.opendaylight.controller.md.sal.common.api.data.AsyncDataBroker.DataChangeScope;
import org.opendaylight.yang.gen.v1.urn.opendaylight.inventory.rev130819.Nodes;
import org.opendaylight.yang.gen.v1.urn.opendaylight.inventory.rev130819.nodes.Node;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.inventory.rev130819.FlowCapableNode;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.inventory.rev130819.tables.Table;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.inventory.rev130819.tables.table.Flow;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.opendaylight.yangtools.yang.binding.DataObject;
import org.opendaylight.controller.md.sal.binding.api.DataBroker;
import org.opendaylight.controller.md.sal.binding.api.DataChangeListener;
import org.opendaylight.yangtools.concepts.ListenerRegistration;
import org.opendaylight.ofconflictd.impl.OfconflictdModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OfconflictdProvider implements BindingAwareProvider, AutoCloseable {

    private static final Logger LOG = LoggerFactory.getLogger(OfconflictdProvider.class);
    private ListenerRegistration<DataChangeListener> registration = null;
    InstanceIdentifier iid = InstanceIdentifier.builder(Nodes.class)
        .child(Node.class).augmentation(FlowCapableNode.class).child(Table.class).child(Flow.class).toInstance();
    OfconflictdModule ofconflict = null;
    DataBroker dataBroker = null;

    @Override
    public void onSessionInitiated(ProviderContext session) {
        LOG.info("OfconflictdProvider Session Initiated");
        dataBroker = session.getSALService(DataBroker.class);
        if (dataBroker != null) {
            registerListener(dataBroker);
        } else {
            LOG.info("DATABROKER is null!!");
        }
    }

    public void registerListener(DataBroker db) {
        ofconflict = new OfconflictdModule(db);
        registration = db.registerDataChangeListener(LogicalDatastoreType.OPERATIONAL,
                iid, ofconflict, DataChangeScope.SUBTREE);
        LOG.info("REGISTRATION COMPLETE");
    }

    @Override
    public void close() throws Exception {
        LOG.info("OfconflictdProvider Closed");
    }

}
