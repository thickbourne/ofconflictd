/*
 * Copyright © 2015 Janakarajan Natarajan and others.  All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */
package org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.ofconflictd.impl.rev141210;

import org.opendaylight.ofconflictd.impl.OfconflictdProvider;

public class OfconflictdModule extends org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.ofconflictd.impl.rev141210.AbstractOfconflictdModule {
    public OfconflictdModule(org.opendaylight.controller.config.api.ModuleIdentifier identifier, org.opendaylight.controller.config.api.DependencyResolver dependencyResolver) {
        super(identifier, dependencyResolver);
    }

    public OfconflictdModule(org.opendaylight.controller.config.api.ModuleIdentifier identifier, org.opendaylight.controller.config.api.DependencyResolver dependencyResolver, org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.ofconflictd.impl.rev141210.OfconflictdModule oldModule, java.lang.AutoCloseable oldInstance) {
        super(identifier, dependencyResolver, oldModule, oldInstance);
    }

    @Override
    public void customValidation() {
        // add custom validation form module attributes here.
    }

    @Override
    public java.lang.AutoCloseable createInstance() {
        OfconflictdProvider provider = new OfconflictdProvider();
        getBrokerDependency().registerProvider(provider);
        return provider;
    }

}
